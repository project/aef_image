Uberimage
=========
To use Uberimage, go to the content type administration page, select "Manage field" of your content type, and add an Uberimage.

Imagecache presets
------------------
Uberimage only support imagecache presets defined with one action of type imagecache_scale_and_crop, with pixels values (not percentage).
One is provided by default by the module, "aef_image_story_image_example".
You can add imagecache presets with the module "Imagecache UI", or programmaticaly. With the UI, go to the "Imagecache" admin, create a new preset, and add a new action of type "Add Scale And Crop". Save it and you are done.

Imagecache presets override
---------------------------
The power of Uberimage is that you can edit the result of a preset in the node edit page. In the node edit page, you can see all the images thumbnails generated with the presets selected in "Imagecache presets to use", that you can recrop yourself with the "Edit" button. You can even replace a thumbnail with a whole new image!!

Imagecache Views Support
------------------------
Contrary to imagefield, it does not come with all the imagecache presets available. Why? Because, on big websites such as France24, it can *dramatically* increase SQL traffic between Mysql and Apache (and I'm speaking about gigabit/s here). So if you want to be able to use one imagecache, first go to admin/settings/aef_image, and select the imagecache presets you want to see available in Views.

Uberimage theme functions
-------------------------
Print an image with the given imagecache preset.
theme('aef_utilities_image', $field_value, $imagecache_preset, $alt = '', $title = '', $attrs = array());
e.g.
theme('aef_utilities_image', $node->field_image[0], 'aef_image_story_image_example');

Print an image plus its infos (title, legend, copyright) with the given imagecache preset.
theme('aef_utilities_image_and_infos', $field_value, $imagecache_preset, $alt = '', $title = '', $attrs = array());
e.g.
theme('aef_utilities_image_and_infos', $node->field_image[0], 'aef_image_story_image_example');

Field use cases
---------------
* Simple upload field:
  Select the "Default imagecache preset to use in the default display" that will be used by default in the node page. If you want to use another preset in another node theme, you will have to use one of the above theme functions. E.g.
function aef_ct_article_preprocess_aef_ct_article_small_teaser_1(&$variables)
{
  if($variables['node']->field_article_image[0]['fid'] > 0 || $variables['node']->field_article_image[0]['nid'] > 0)
  {
    $variables['image_small_thumbnail'] = 
      l(theme('aef_utilities_image', $variables['node']->field_article_image[0],
        'aef_ct_article_small_thumbnail'), 
        'node/' . $variables['node']->nid, 
        array('html' => true));
  }
}

* Simple upload field with format selection:
  In this mode, after the upload, you can select the format to use in the node edit page. Select the "Imagecache presets to use" that will be selectable, and the one selected by default will be the one choosed in "Default imagecache preset to use in the default display". Useful mode in some cases, e.g. when you want to make diaporama of different sizes.

* Nodereference mode:
  In this mode you can reference any node which has a field of type Uberimage upload. If you happen to reference a node with a Uberimage nodereference, it will look recursively for the node with a Uberimage upload field.
  In this mode, all the thumbnail modifications plus the title/legend/credits will be loaded, and you can edit them WITHOUT changing the original node! That's a very interesting feature when you want to manage a collection of images.

* Nodereference mode with format selection:
  Likewise, reference a node with a uberimage upload field, and let the user choose the preset to display in the node edit page.

Interesting use cases
---------------------
* Image collection:
  Create a new content type called "Image", only add a Uberimage upload field. Make a View to browse within your images. Then on your story content type, add a Uberimage nodereference field. Using this View, your editor will be able to search and add an image to your story.
  For the teasers of stories (small teasers to be displayed on block or elsewhere), I strongly recommend you to do them as CCK Node formatter, since these node themes are easily reusable. You can there use the Uberimage theming functions as shown above.
  Pushing things further, you can use the module Externodes to store your image nodes in a remote Drupal, that can be plugged to other Drupal, making your image collection reusable easily. WARNING, that is a more advanced feature that can be *very* tricky to set up.
