/**
 * Let's add the crop behavior
 */
Drupal.behaviors.aef_image_crop = function() {

  // Add the necessary JS for the JS crop
  $('.aef-image-crop-image:not(aef_image_crop-processed)').addClass('aef_image_crop-processed').each(function() {

    var args = new Array();
    var id = $(this).attr('id');
    if(Drupal.settings.jcrop[id] != undefined)
      args = Drupal.settings.jcrop[id].args;

    args['onChange'] = convertStringToFunction(args['onChange']);
    args['onSelect'] = convertStringToFunction(args['onSelect']);

    args['origId'] = id;

    $(this).Jcrop(args);

  });

  // When selecting a format in a given delta in a field, select the same format for each others delta
  $('.aef_image_wrapper:not(aef_image_wrapper-processed)').addClass('aef_image_wrapper-processed').each(function() {
    var field_name = $(this).attr('field_name');
    var id = $(this).attr('id');

    $(this).find('input.aef_image_preset_selection').click(function() {
      var selected_preset = $(this).attr('value');

      $('.aef_image_wrapper[field_name=' + field_name + '] input.aef_image_preset_selection[value=' + selected_preset + ']').attr("checked", "checked");
    });

  });

}

function aef_image_update_coords(c)
{
  $('.' + this.origId + '_x').val(c.x);
  $('.' + this.origId + '_y').val(c.y);
  $('.' + this.origId + '_x2').val(c.x2);
  $('.' + this.origId + '_y2').val(c.y2);

  var preview = $('#'  + this.origId + '_preview');  
  var rx = preview.attr('preset_width') / c.w;
  var ry = preview.attr('preset_height') / c.h;


  preview.css({ 
    width: Math.round(rx * preview.attr('img_width')) + 'px',
    height: Math.round(ry * preview.attr('img_height')) + 'px',
    marginLeft: '-' + Math.round(rx * c.x) + 'px',
    marginTop: '-' + Math.round(ry * c.y) + 'px'
  });
}

/**
 * Convert a string containing a function name into this function
 */
function convertStringToFunction(str)
{
  if(typeof(str) == 'string')
  {
    var fn = window[str];
    if(typeof fn === 'function') {
        str = fn;
    }
  }

  return str;
}


